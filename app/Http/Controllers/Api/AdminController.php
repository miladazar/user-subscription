<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserSubscription;

class AdminController extends Controller
{
    public function getLatestExpiredSubscriptions(Request $request)
    {
        $latestRecord = UserSubscription::latest()->first();

        if ($latestRecord) {
            return response()->json(['expired_count' => $latestRecord->expired_count, 'recorded_at' => $latestRecord->recorded_at]);
        } else {
            return response()->json(['message' => 'No records found'], 404);
        }
    }
}
