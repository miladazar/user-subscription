<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Date;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\UserSubscription;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminAlert;


class CheckGooglePlaySubscriptionStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $application;
    protected $userSubscription;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user ,$application)
    {
        $this->user = $user;
        $this->application = $application;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $googlePlaySubscriptionUrl =  $this->application->platformService()->url;

        $response = Http::get($googlePlaySubscriptionUrl."?user_id=$this->user&app_id=$this->application");  //chanage parameter to get Subscription status

        if ($response->status() != 200) {
            Log::error(' subscription status check failed for URL: ' . $this->googlePlaySubscriptionUrl . '. Retrying in 1 hour.');
            //service must repeat checking 1 hour later to get the result.
            $this->release(Date::now()->addHours(1));
        } else {
              $data = $response->json(); 
              $status = $data['status'];
              
              $subscription = UserSubscription::updateOrCreate(
                    ['user_id' => $this->user->id, 'application_id' => $this->application->id , 'status' => $status]
              );

              $originalStatus = $subscription->getOriginal('status');
              $currentStatus = $subscription->status;
              
              //Changing subscription status from ‘active’ to ‘expired’ must be reported to admin via email
              if ($originalStatus == 'Active' && $currentStatus =='Expired') {
                     Mail::to("admin@gmail.com")  //change admin email or get from env file
                         ->send(new AdminAlert($this->user->name,$this->application->name));
               }
        }
    }
}
