<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminAlert extends Mailable
{
    use Queueable, SerializesModels;

    protected $userName;
    protected $applicationName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName ,$applicationName)
    {
        $this->userName = $userName;
         $this->applicationName = $applicationName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.changestatus')
                    ->with([
                        'userName' => $this->userName,
                        'applicationName' => $this->applicationName,
                        // Other data to pass to the view
                    ]);
    }
}
