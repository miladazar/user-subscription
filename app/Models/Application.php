<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'platform_id',
        'name',
        'price',
        'description',
    ];

    public function platformService()
    {
        return $this->belongsTo(Platform::class, 'platform_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
