<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpiredSubscription extends Model
{
    use HasFactory;

     protected $fillable = [
        'expired_count',
        'recorded_at',
    ];
}
