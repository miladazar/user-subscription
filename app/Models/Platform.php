<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'url',
        'token',
        'username',
        'password',
        'callback',
    ];

     public function applications()
    {
        return $this->hasMany(application::class);
    }
}
