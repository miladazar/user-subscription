<?php

namespace App\Enums;

// enum StatusSubscription :  string {
//     case Pending = 'pending';
//     case Active = 'active';
//     case Inactive = 'inactive';
// }

final class StatusSubscription extends Enum
{
    const Pending = 0;
    const Active = 1;
    const Expired = 2;
}