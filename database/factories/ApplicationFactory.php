<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Platform;

class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $platformIds = Platform::all()->pluck('id')->toArray();
        return [
            'platform_id'=> $this->faker->randomElement($platformIds) ,
            'name'=>$this->faker->name(),
            'price'=>$this->faker->randomNumber(5, true),
            'description'=>$this->faker->name(),
        ];
    }
}
