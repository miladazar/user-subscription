<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\User;
use App\Platform;

class UserSubscriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userIds = User::all()->pluck('id')->toArray();
        $ApplicationIds = Platform::all()->pluck('id')->toArray();
        $status = ['Pending','Active','Expired'];
        return [
            'user_id'=> $this->faker->randomElement($userIds) ,
            'application_id'=>$this->faker->randomElement($ApplicationIds) ,
            'status'=>$this->faker->randomElement($status),
        ];
    }
}
