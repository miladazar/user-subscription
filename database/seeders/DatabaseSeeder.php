<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlatformTableSeeder::class);

        \App\Models\User::factory(20)->create();

        \App\Models\Application::factory(10)->create();

         \App\Models\UserSubscription::factory(15)->create();
    }
}
