<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Platform;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert your data here using the DB facade
        Platform::create([
            'name'=>'Google Play',
            'url'=>'https://api.google.com/subscriptions',
            'token'=>'cUagdMjP9MpBoKlZuBoN2n6JDowV61MPdpgtfarplPk7jK',
            'username'=>'test',
            'password'=>'1234',
            'callback'=>'/callback',
        ],[
            'name'=>'Play Store',
            'url'=>'https://api.apple.com/subscriptions',
            'token'=>'dHg7kmfjwqT8uOHWFHyuA580d1BhysFy9eyTmS7zmdHygWnNIi',
            'username'=>'test',
            'password'=>'1234',
            'callback'=>'/callback',
        ]);
    }
}
