<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function get_response_data()
    {
        $response = $this->get('/latest-expired-subscriptions');

        $response->assertStatus(200);
    }


    /** @test */
    public function request_as_post()
    {
        $response = $this->post(route('/latest-expired-subscriptions'));
        $response->assertStatus(415);
    }
}
